import os
import sys
import hashlib
import subprocess

import settings
from loghelper import cf, say, warn, error, msg


def calculateFullHash(infile):
    msg(cf(), "calculating full hash {}", infile)
    bufsize = 65536  # 64kb
    hashx = hashlib.sha1() if settings.UseSHA1 else hashlib.md5()

    with open(infile, 'rb') as f:
        while True:
            data = f.read(bufsize)
            if not data: break
            hashx.update(data)

    msg(cf(), "hash calculation done {}", infile)
    return hashx.hexdigest()


def quickHash(infile):
    # gen hash using the first and last 1 MB
    hashx = hashlib.sha1()
    bufsize = 1024 * 1024
    with open(infile, 'rb') as f:
        hashx.update(f.read(bufsize))
        # go to end
        f.seek(0, os.SEEK_END)
        if f.tell() > bufsize:
            f.seek(-bufsize, os.SEEK_END)
            hashx.update(f.read(bufsize))

    return hashx.hexdigest()


def listWorkDir():
    settings.WorkFiles = os.listdir(settings.WorkDir)


def printWorkFiles():
    print("WORKING DIR: {}\n".format(settings.WorkDir))
    for i, f in enumerate(settings.WorkFiles):
        path = os.path.join(settings.WorkDir, f)
        try:
            if os.path.isdir(path):
                uprint("DIR  [{}{}] {}".format(settings.ShortcutChar, i, f))
            else:
                uprint("FILE [{}{}] {}".format(settings.ShortcutChar, i, f))

        except UnicodeEncodeError:
            warn(cf(), "non English name")


def printStackFiles():
    print("::STACK::")
    for i, path in enumerate(settings.UserStack):
        f = os.path.basename(path)
        try:
            if os.path.isdir(path):
                uprint("DIR  [{}{}] {}".format(settings.ShortcutStackChar, i, f))
            else:
                uprint("FILE [{}{}] {}".format(settings.ShortcutStackChar, i, f))

        except UnicodeEncodeError:
            warn(cf(), "non English name")


def idxWorkFile(idx):
    if idx.startswith(settings.ShortcutChar):
        no = idx.split(settings.ShortcutChar)[1].strip()
        try:
            no = int(no)
        except:
            error(cf(), "wrong index {}", no)
            raise ValueError("Wrong index", no)
        else:
            if -len(settings.WorkFiles) < no < len(settings.WorkFiles):
                msg(cf(), "cwd [{}]: {}", idx, settings.WorkFiles[no])
                return settings.WorkFiles[no]
            else:
                raise ValueError("Wrong index", no)
    return idx


def idxStackFile(idx):
    if idx.startswith(settings.ShortcutStackChar):
        no = idx.split(settings.ShortcutStackChar)[1].strip()
        try:
            no = int(no)
        except:
            error(cf(), "wrong stack index {}", no)
            raise ValueError("Wrong stack index", no)
        else:
            if -len(settings.UserStack) < no < len(settings.UserStack):
                msg(cf(), "stack [{}]: {}", idx, settings.UserStack[no])
                return settings.UserStack[no]
            else:
                raise ValueError("Wrong stack index", no)
    return idx


def changeWorkDir(newdir):
    if os.path.isfile(newdir):
        newdir = os.path.dirname(newdir)

    if os.path.isdir(newdir):
        settings.WorkDir = newdir
        msg(cf(), "working dir {}", newdir)
        listWorkDir()
        printWorkFiles()
    else: raise FileNotFoundError("Directory does not exist", newdir)


def addToStack(path):
    if path not in settings.UserStack:
        settings.UserStack.append(path)
        #msg(cf(), "stack added: {}", path)


class parseCommand:
    command = None
    args = []

    def __init__(self, cmd, expectedargs=1):
        cmd = cmd.strip()

        self.args = []
        self.command = cmd.split()[0]

        for a in cmd.split(' ', expectedargs):
            self.args.append(a.strip())
        if not len(self.args) > expectedargs:
            raise ValueError("Insufficient Arguments", cmd)


def openDirInWinExplorer(dirpath):
    if os.path.isdir(dirpath):
        subprocess.Popen('explorer "{0}"'.format(dirpath))
    else:
        msg(cf(), "not a valid directory {}", dirpath)

def showInWinExplorer(path):
    if os.path.exists(path):
        subprocess.Popen('explorer /select, "{0}"'.format(path))
    else:
        msg(cf(), "not exists {}", path)


def printHelp():
    for key in sorted(settings.Commands):
        commands = settings.Commands[key][0]
        description = settings.Commands[key][1]

        print("{0}\n{1}\n".format(commands[0].upper(), description))

def printSpecificHelp(cmd):
    cmd = cmd.replace('_', ' ')
    for key in settings.Commands:
        commands = settings.Commands[key][0]
        description = settings.Commands[key][1]
        if cmd in commands:
            for c in commands:
                print(c.upper(), end=", ")
            print("\n\n{0}".format(description))
            return
    print("Command '{0}' not found. Try 'help {0}_' or enter 'help' to show a list of commands.".format(cmd))


def uprint(*objects, sep=' ', end='\n', file=sys.stdout):
    # handles unicode printing
    enc = file.encoding
    if enc == 'UTF-8':
        print(*objects, sep=sep, end=end, file=file)
    else:
        f = lambda obj: str(obj).encode(enc, errors='backslashreplace').decode(enc)
        print(*map(f, objects), sep=sep, end=end, file=file)
