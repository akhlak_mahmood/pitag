import os
import loghelper as Log
import settings
import interactive
from database import allFiles, allTags

if __name__ == "__main__":
    print("""PiTag version 1.0""")
    # database summary
    print(
        """Files: {}\tTags: {}"""
        """\n==========================
        """.format(len(allFiles()), len(allTags())))

    # initialize()
    Log.set_tag("PiTag")

    #Log.turn_on()

    settings.WorkDir = os.getcwd()
    settings.WorkFiles = os.listdir(settings.WorkDir)

    Log.msg(Log.cf(), "environment initialized")

    interactive.run()
