import datetime
import os

from sqlalchemy.orm import sessionmaker
from sqlalchemy import or_

import loghelper as Log
from loghelper import cf
from schema import fObject, Tag, Relation, Hierarchy, Engine
from utils import quickHash

session = sessionmaker(bind=Engine)()
Log.msg(cf(), "database session initialized")


def commit():
    session.commit()
    Log.msg(cf(), "DATABASE COMMIT")


def addNewFile(path):
    newfile = fObject(path=path)
    newfile.added = datetime.datetime.now().strftime("%B %d, %Y %I:%M %p")
    if os.path.isdir(path):
        newfile.isdir = 1
    else:
        newfile.hash = quickHash(newfile.path)
        newfile.isdir = 0

    session.add(newfile)
    Log.msg(cf(), "new file added {}", path)
    return newfile


def addFile(path):
    filehash = quickHash(path)
    fileO = session.query(fObject).filter(fObject.hash == filehash)
    fileOsel = fileO.first()

    if fileOsel:
        # file with same hash found in database

        if fileOsel.path == path:
            # saved old path is same as new path
            Log.msg(cf(), "already in database {}", path)
            return fileOsel

        else:
            # saved old path is not same as new path

            if os.path.isfile(fileOsel.path):
                # file was copied, add the new one
                Log.msg(cf(), "file was copied {} to {}", fileOsel.path, path)
                nfileO = addNewFile(path)

                # copy the tags of old file to new one
                tags = findTagsOfFile(fileOsel)
                for t in tags:
                    addRelation(nfileO, t)

                return nfileO

            else:
                # file was moved
                Log.msg(cf(), "file was moved {} to {}", fileOsel.path, path)
                fileO.update({fObject.path: path})
                Log.msg(cf(), "old path updated {}", path)
                return fileO.first()
    else:
        return addNewFile(path)


def deleteFile(obj):
    if isinstance(obj, fObject):
        fileO = session.query(fObject).filter(fObject.id == obj.id).first()
    else:
        fileO = session.query(fObject).filter(fObject.path == obj).first()

    if fileO:
        session.delete(fileO)
        Log.msg(cf(), "database file deleted {}", fileO.path)
    else:
        Log.warn(cf(), "file not in database {}", fileO.path)


def copytags(srcpath, tarpath):
    srcfile = addFile(srcpath)
    tarfile = addFile(tarpath)
    tags = findTagsOfFile(srcfile)

    for tag in tags:
        addRelation(tarfile, tag)
    Log.msg(cf(), "tags copied: {}", [t.name for t in tags])
    return tags


def addTag(tname, **kwargs):
    tagO = session.query(Tag).filter(Tag.name == tname).first()
    if tagO:
        Log.msg(cf(), "{} already in database", tname)
        return tagO

    newtag = Tag(name=tname)
    if "desc" in kwargs:
        newtag.desc = kwargs['desc']
    session.add(newtag)
    Log.msg(cf(), "{} added to database", tname)
    if "parent" in kwargs:
        addHierarchy(kwargs['parent'], newtag)
    return newtag


def deleteTag(tag):
    if isinstance(tag, Tag):
        tagO = session.query(Tag).filter(Tag.id == tag.id).first()
    else:
        tagO = session.query(Tag).filter(Tag.name == tag).first()
    if tagO:
        session.delete(tagO)
        Log.say(cf(), "{} deleted from database", tagO.name)
    else:
        Log.warn(cf(), "{} does not exist in database", tag)


def updateTagName(tag, newtag):
    # if findTagWithName(newtag):
    #     raise ValueError("A tag with the same name already exists in database.")
    if isinstance(tag, Tag):
        session.query(Tag).filter(Tag.id == tag.id).update({Tag.name: newtag})
    else:
        session.query(Tag).filter(Tag.name == tag).update({Tag.name: newtag})
    Log.say(cf(), "tag {} updated to {} in database.", tag, newtag)


def addHierarchy(parent_tag, child_tag):
    if not isinstance(parent_tag, Tag):
        parent_tag = session.query(Tag).filter(Tag.name == parent_tag).first()
        if not parent_tag:
            raise ValueError("No such parent in database!")
    if not isinstance(child_tag, Tag):
        child_tag = session.query(Tag).filter(Tag.name == child_tag).first()
        if not child_tag:
            raise ValueError("No such child in database!")

    hierO = session.query(Hierarchy) \
        .filter(Hierarchy.parent == parent_tag, Hierarchy.child == child_tag).first()
    if hierO:
        return hierO

    newhier = Hierarchy(parent=parent_tag, child=child_tag)
    session.add(newhier)
    return newhier


def deleteHierarchy(parent_tag, child_tag):
    if not isinstance(parent_tag, Tag):
        parent_tag = session.query(Tag).filter(Tag.name == parent_tag).first()
    if not isinstance(child_tag, Tag):
        child_tag = session.query(Tag).filter(Tag.name == child_tag).first()

    hierO = session.query(Hierarchy) \
        .filter(Hierarchy.parent == parent_tag, Hierarchy.child == child_tag).first()
    if hierO:
        session.delete(hierO)
        Log.say(cf(), "{} --> {} hierarchy deleted from database.", parent_tag.name, child_tag.name)
    else:
        Log.warn(cf(), "{} --> {} no such hierarchy in database.", parent_tag.name, child_tag.name)


def addRelation(obj, tag):
    if not isinstance(obj, fObject):
        # if not fObject, it must be file path
        obj = session.query(fObject).filter(fObject.path == obj).first()
        if not obj:
            raise ValueError("No such file in database.")
    if not isinstance(tag, Tag):
        tag = session.query(Tag).filter(Tag.name == tag).first()
        if not tag:
            raise ValueError("No such tag in database.")

    relO = session.query(Relation) \
        .filter(Relation.object == obj, Relation.tag == tag).first()
    if relO:
        Log.msg(cf(), "Relation {} <--> {} already in database.", obj.path, tag.name)
        return relO

    rel = Relation(object=obj, tag=tag)
    session.add(rel)
    Log.msg(cf(), "Relation {} <--> {} added to database.", obj.path, tag.name)
    return rel


def deleteRelation(obj, tag):
    if not isinstance(obj, fObject):
        # if not fObject, it must be file path
        obj = session.query(fObject).filter(fObject.path == obj).first()
        if not obj:
            Log.warn(cf(), "{} No such file in database.", obj)
            return
    if not isinstance(tag, Tag):
        tag = session.query(Tag).filter(Tag.name == tag).first()
        if not tag:
            Log.warn(cf(), "{} No such tag in database.", tag)
            return

    relO = session.query(Relation) \
        .filter(Relation.object == obj, Relation.tag == tag).first()
    if relO:
        session.delete(relO)
        Log.msg(cf(), "Relation {} <--> {} deleted from database.", obj.path, tag.name)
    else:
        Log.warn(cf(), "Relation {} <--> {} does not exist in database.", obj.path, tag.name)


def findTagWithName(name):
    return session.query(Tag).filter(Tag.name == name).first()


def findFileWithPath(path):
    return session.query(fObject).filter(fObject.path == path).first()


def findRelation(obj, tag):
    return session.query(Relation) \
        .filter(Relation.object == obj, Relation.tag == tag).first()


def findFilesWithTag(tag):
    if not isinstance(tag, Tag):
        return findFilesWithTagName(tag)
    rels = session.query(Relation).filter(Relation.tag == tag).all()
    return [f.object for f in rels]


def findFilesWithAnyTag(tagnames):
    tags = []
    for t in tagnames:
        t = findTagWithName(t)
        if t: tags.append(t)

    if len(tags):
        ress = session.query(Relation) \
            .filter(or_(Relation.tag == tag for tag in tags)).all()
        return [f.object for f in ress]
    else:
        return []


def findFilesWithAllTags(tagnames):
    tags = []
    for t in tagnames:
        t = findTagWithName(t)
        if t: tags.append(t)

    if len(tags) == 1:
        return findFilesWithTag(tags[0])

    elif len(tags) > 1:
        fileObs = []

        for tag in tags:
            rels = session.query(Relation).filter(Relation.tag == tag).all()
            fileObs.append([r.object for r in rels])

        commons = fileObs[0]
        for flist in fileObs[1:]:
            commons = [val for val in commons if val in flist]

        return commons

    else:
        return []


def findFilesWithTagName(name):
    tag = findTagWithName(name)
    if not tag: return []
    return findFilesWithTag(tag)


def allFiles():
    return session.query(fObject).all()


def allTags():
    return session.query(Tag).all()


def findTagsOfFile(obj):
    if not isinstance(obj, fObject):
        return findTagsOfPath(obj)
    return [t.tag for t in session.query(Relation).filter(Relation.object == obj).all()]


def findTagsOfPath(path):
    obj = findFileWithPath(path)
    if not obj: return []
    return findTagsOfFile(obj)


def findParents(tag):
    if not isinstance(tag, Tag):
        tag = findTagWithName(tag)
    if not tag: return []
    rels = session.query(Hierarchy).filter(Hierarchy.child == tag).all()
    return [h.parent for h in rels]


def findChildren(tag):
    if not isinstance(tag, Tag):
        tag = findTagWithName(tag)
    if not tag: return []
    rels = session.query(Hierarchy).filter(Hierarchy.parent == tag).all()
    return [h.child for h in rels]


"""
Log.turn_on()
f = addFile("C:\\Log.py")
pt = addTag("example")
ct = addTag("simple", desc="A simple example", parent=pt)
addRelation(f, ct)
f = addFile("E:\\Barbie")
addRelation(f, pt)
addRelation(f, ct)

print("Done!")
q = session.query
"""
