import os
import traceback

import sys

import settings
from settings import Commands
from loghelper import cf, say, warn, error, set_debug
from handlers import *
from dbhandlers import *


def run():
    while True:
        try:
            cmd = input(settings.InputPrompt)
            print("")
            cmd = cmd.strip()


            if cmd.lower() in Commands['exit'][0]:
                say(cf(), "Exit")
                break


            if cmd.startswith(settings.ShortcutChar):
                it = idxWorkFile(cmd)
                it = os.path.join(settings.WorkDir, it)
                if os.path.isdir(it):
                    print(it)
                    changeWorkDir(it)
                elif os.path.isfile(it):
                    os.startfile(it)

            elif cmd.startswith(settings.ShortcutStackChar):
                it = idxStackFile(cmd)
                if os.path.isdir(it):
                    print(it)
                    changeWorkDir(it)
                elif os.path.isfile(it):
                    os.startfile(it)

            elif cmd.lower() in Commands['help'][0]:
                printHelp()

            elif cmd.lower().startswith(Commands['help_'][0]):
                handle_printhelp(cmd)

            elif cmd.lower() in Commands['cd'][0]:
                print(settings.WorkDir)

            elif cmd.lower().startswith(Commands['cd_'][0]):
                handle_cd(cmd)

            elif cmd.lower() in Commands['debug'][0]:
                set_debug(True)

            elif cmd.lower() in Commands['debugoff'][0]:
                set_debug(False)

            # list, dir, ls, files
            elif cmd.lower() in Commands['list'][0]:
                listWorkDir()
                printWorkFiles()

            # start
            elif cmd.lower().startswith(Commands['execute_'][0]):
                handle_execute(cmd)

            # clear screen
            elif cmd.lower() in Commands['cls'][0]:
                os.system("cls")

            elif cmd.lower() in Commands['info'][0]:
                handle_showinfo()

            elif cmd.lower().startswith(Commands['fileinfo_'][0]):
                handle_showfileinfo(cmd)

            elif cmd.lower().startswith(Commands['mkdir_'][0]):
                handle_newdir(cmd)

            elif cmd in Commands['..'][0]:
                handle_cd("cd ..")

            elif cmd.lower().startswith(Commands['add_'][0]):
                handle_addfile(cmd)

            elif cmd.lower().startswith(Commands['tag_'][0]):
                handle_tagfile(cmd)

            elif cmd.lower().startswith(Commands['deletetag_'][0]):
                handle_deletetag(cmd)

            elif cmd.lower().startswith(Commands['copytag_'][0]):
                handle_copytags(cmd)

            elif cmd.lower().startswith(Commands['tagstack_'][0]):
                handle_tagallstack(cmd)

            elif cmd.lower().startswith(Commands['tagall_'][0]):
                handle_tagall(cmd)

            elif cmd.lower() in Commands['showstack'][0]:
                printStackFiles()

            elif cmd.lower().startswith(Commands['del_'][0]):
                handle_stackdel(cmd)

            elif cmd.lower() in Commands['clear stack'][0]:
                settings.UserStack = []
                say(cf(), "clear stack")

            elif cmd.lower() in Commands['paste stack'][0]:
                from shutil import copy2
                for f in settings.UserStack:
                    copy2(f, os.path.join(settings.WorkDir, os.path.basename(f)))
                say(cf(), "copy stack")

            elif cmd.lower().startswith(Commands['savestack_'][0]):
                handle_savestack(cmd)

            elif cmd.lower().startswith(Commands['tags_'][0]):
                handle_filetags(cmd)

            elif cmd.lower().startswith(Commands['files_'][0]):
                handle_tagfiles(cmd)

            elif cmd.lower().startswith(Commands['alltags_'][0]):
                handle_alltagsfiles(cmd)

            elif cmd.lower().startswith(Commands['anytag_'][0]):
                handle_anytagfiles(cmd)

            elif cmd.lower().startswith(Commands['rename_'][0]):
                handle_rename(cmd)
                listWorkDir()

            elif cmd.lower().startswith(Commands['delete_'][0]):
                handle_delete(cmd)
                listWorkDir()

            elif cmd.lower().startswith(Commands['surerename_'][0]):
                handle_rename(cmd, True)
                listWorkDir()

            elif cmd.lower().startswith(Commands['explorer_'][0]):
                handle_explorer(cmd)

            elif cmd.lower() in Commands['explore'][0]:
                say(cf(), "opening Windows Explorer")
                openDirInWinExplorer(settings.WorkDir)

            elif cmd.lower() in Commands['refreshdb'][0]:
                handle_refreshdb()

            else:
                print("Unknown Command")

        except Exception as err:
            error(cf(), "command execution failed: {}", err)
            msg(cf(), "Python {}", traceback.format_exc())
