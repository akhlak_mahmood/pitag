import os
import settings
from loghelper import cf, say, warn, error
from utils import *
import database


def handle_cd(cmd):
    newpath = parseCommand(cmd, 1).args[1]

    if newpath in ("..", "."):
        newpath = os.path.dirname(settings.WorkDir)
    elif newpath.startswith(settings.ShortcutStackChar):
        newpath = idxStackFile(newpath)
    elif newpath.startswith(settings.ShortcutChar):
        newpath = idxWorkFile(newpath)
        newpath = os.path.join(settings.WorkDir, newpath)
    else:
        newpath = os.path.join(settings.WorkDir, newpath)

    changeWorkDir(newpath)


def handle_execute(cmd):
    file = parseCommand(cmd, 1).args[1]
    if file.startswith(settings.ShortcutStackChar):
        file = idxStackFile(file)
    else:
        file = idxWorkFile(file)
        file = os.path.join(settings.WorkDir, file)

    if os.path.isfile(file):
        os.startfile(file)
    else:
        error(cf(), "not a file: {}", file)


def handle_stackdel(cmd):
    idx = parseCommand(cmd, 1).args[1]
    if not idx.startswith(settings.ShortcutStackChar):
        error(cf(), "wrong stack id {}", idx)
    else:
        item = idxStackFile(idx)
        settings.UserStack.remove(item)
        printStackFiles()


def handle_newdir(cmd):
    newd = parseCommand(cmd, 1).args[1]

    try:
        os.mkdir(os.path.join(settings.WorkDir, newd))
    except:
        error(cf(), "failed to create dir {}", newd)
    else:
        listWorkDir()
        say(cf(), "created dir {}", newd)


def handle_rename(cmd, sure=False):
    cmd = parseCommand(cmd, 2)
    srcn = cmd.args[1]
    dst = cmd.args[2]

    if srcn.startswith(settings.ShortcutStackChar):
        srcp = idxStackFile(srcn)
        src = os.path.basename(srcp)
        ext = os.path.splitext(src)[1]
        dstp = os.path.join(os.path.dirname(srcp), dst + ext)
    else:
        src = idxWorkFile(srcn)
        srcp = os.path.join(settings.WorkDir, src)
        ext = os.path.splitext(src)[1]
        dstp = os.path.join(settings.WorkDir, dst + ext)

    if not sure:
        uprint("Rename {} to {}{}?".format(src, dst, ext))
        if input("Press only Enter to confirm.") != "":
            return

    try:
        os.rename(srcp, dstp)
    except Exception as err:
        error(cf(), err)
    else:
        database.addFile(dstp)
        database.commit()

        if srcn.startswith(settings.ShortcutChar):
            no = settings.WorkFiles.index(src)
            settings.WorkFiles[no] = dst + ext
        elif srcn.startswith(settings.ShortcutStackChar):
            no = settings.UserStack.index(srcp)
            settings.UserStack[no] = dstp

        say(cf(), "renamed: {}", dst)


def handle_delete(cmd, sure=False):
    cmd = parseCommand(cmd, 1)
    fname = cmd.args[1]

    if fname.startswith(settings.ShortcutStackChar):
        fpath = idxStackFile(fname)
        fname = os.path.basename(fpath)
    else:
        fname = idxWorkFile(fname)
        fpath = os.path.join(settings.WorkDir, fname)

    if not sure:
        uprint("Delete file {}?".format(fname))
        if input("Press only Enter to confirm.") != "":
            return

    try:
        os.remove(fpath)
    except Exception as err:
        error(cf(), err)
    else:
        fileO = database.findFileWithPath(fpath)
        if fileO:
            database.deleteFile(fileO)
            database.commit()
            say(cf(), "deleted {} from database", fname)

        if fname.startswith(settings.ShortcutStackChar):
            settings.UserStack.remove(fpath)

        say(cf(), "deleted: {}", fpath)


def handle_explorer(cmd):
    cmd = parseCommand(cmd, 1)
    fname = cmd.args[1]

    if fname.startswith(settings.ShortcutStackChar):
        fpath = idxStackFile(fname)
        fname = os.path.basename(fpath)
    else:
        fname = idxWorkFile(fname)
        fpath = os.path.join(settings.WorkDir, fname)

    say(cf(), "opening Windows Explorer to {}", fpath)
    showInWinExplorer(fpath)


def handle_printhelp(cmd):
    commandname = parseCommand(cmd, 1).args[1]
    for c in commandname.split():
        printSpecificHelp(c)

def handle_savestack(cmd):
    cmd = parseCommand(cmd, 1)
    fname = cmd.args[1]

    fpath = os.path.join(settings.WorkDir, fname)
    if not os.path.isfile(fpath):
        fp = open(fpath, 'w+')
        for item in settings.UserStack:
            fp.write(item + '\n')
        fp.close()
        say(cf(), "stack saved to {}", fpath)
    else:
        error(cf(), "{} already exists", fpath)

