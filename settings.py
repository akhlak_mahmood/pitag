# globals configs

UseSHA1                 = True
DBFile                  = "PiTag.db"
ShortcutChar            = ";"
ShortcutStackChar       = ":"
InputPrompt             = "\n>>> "

WorkDir     = None
WorkFiles   = []
Engine      = None

UserStack   = []


Commands   = {
    'help': (
        ('help', '?', 'command'),
        "Displays help information for all available commands.\nFor more information for a specific command type HELP command-name."
    ),
    'help_': (
        ('helpon ', 'help ', 'command '),
        "Displays help information on a specific command."
    ),
    'exit': (
        ('exit', 'q', 'quit', 'exit()', 'quit()'),
        "Exit program."
    ),
    'cd': (
        ('cwd', 'currentdir', 'cd', 'path', 'currdir'),
        "Displays current working directory."
    ),
    'cd_': (
        ('cd ', 'goto ', 'enter '),
        "Change working directory"
    ),
    'debug': (
        ('debug', 'debug on'),
        "Turn on debug messages. Shows detailed info on processes and handlers."
    ),
    'debugoff': (
        ('debug off', 'debugoff'),
        "Turn off debug messages."
    ),
    'list': (
        ('ls', 'list', 'dir', 'listdir'),
        "Displays a list of files and directories in the current working directory."
    ),
    'execute_': (
        ('run ', 'execute ', 'start '),
        "Execute/open file."
    ),
    'cls': (
        ('cls', 'clrscr', 'clear screen'),
        "Clear screen."
    ),
    'info': (
        ('info', 'view', 'database'),
        "Displays database info."
    ),
    'fileinfo_': (
        ('fileinfo ', 'showinfo '),
        "Displays details on a file."
    ),
    'mkdir_': (
        ('mkdir ', 'mk ', 'newdir '),
        "Create a new directory."
    ),
    '..': (
        ('..', '.'),
        "Goto parent directory."
    ),
    'add_': (
        ('addfile ', 'add '),
        "Add or update a file to database."
    ),
    'tag_': (
        ('tag ', 'tagfile '),
        "Add one or more tags to file."
    ),
    'deletetag_': (
        ('deltag ', 'deletetag '),
        "Delete one or more tags from a file."
    ),
    'copytag_': (
        ('copytags ', 'copytag '),
        "Copy all tags of one file to other."
    ),
    'tagstack_': (
        ('tagstack ', 'tagstackall '),
        "Add one or more tags to all files in the stack."
    ),
    'tagall_': (
        ('tagall ', 'tagcwd '),
        "Add one or more tags to all files of the current working directory."
    ),
    'showstack': (
        ('stack', 'showstack'),
        "Displays the items of the stack."
    ),
    'del_': (
        ('del ', 'stackdel '),
        "Delete an item of the stack."
    ),
    'clear stack': (
        ('clrst', 'clear stack', 'stack clear', 'cln', 'clst'),
        "Clear the stack."
    ),
    'paste stack': (
        ('paste stack', 'stack paste'),
        "Paste the items of stack to current working directory."
    ),
    'savestack_': (
        ('savestack ', 'stacksave ', 'stackdump ', 'dumpstack '),
        "Save the file paths of the stack in a text file."
    ),
    'tags_': (
        ('tagsof ', 'tags '),
        "Displays all tags of a file."
    ),
    'files_': (
        ('filesof ', 'files '),
        "Displays all files having a tag."
    ),
    'alltags_': (
        ('find ', 'withalltags ', 'alltags '),
        "Displays the files having all the specified tags."
    ),
    'anytag_': (
        ('search ', 'withanytag ', 'anytag '),
        "Displays the files having any of the specified tags."
    ),
    'rename_': (
        ('rename ', 'ren '),
        "Rename a file."
    ),
    'delete_': (
        ('delete ', 'remove '),
        "Delete a file from stack or current working directory."
    ),
    'suredelete_': (
        ('suredel ', 'suredelete ', 'sureremove '),
        "Delete a file from stack or current working directory without asking confirmation."
    ),
    'surerename_': (
        ('sureren ', 'surerename ', 'rensure ', 'renamesure '),
        "Rename a file without asking for confirmation."
    ),
    'explorer_': (
        ('explore ', 'explorer '),
        "Open a file in the Windows Explorer."
    ),
    'explore': (
        ('explore', 'explorer'),
        "Open the current working directory in the Windows Explorer."
    ),
    'refreshdb': (
        ('refreshdb', 'refreshdatabase'),
        "Remove deleted files from database."
    ),
}

