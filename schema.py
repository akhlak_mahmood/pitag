from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref
from sqlalchemy import create_engine

import settings
import loghelper as Log

Base = declarative_base()


class fObject(Base):
    __tablename__ = 'fobject'

    id = Column(Integer, primary_key=True)
    path = Column(String(512), nullable=False)
    isdir = Column(Integer, default=0)
    hash = Column(String(128))
    added = Column(String(30))
    deleted = Column(String(30))
    modified = Column(String(30))


class Tag(Base):
    __tablename__ = 'tag'

    id = Column(Integer, primary_key=True)
    name = Column(String(50))
    desc = Column(String(1000))


class Hierarchy(Base):
    __tablename__ = "hierarchy"

    id = Column(Integer, primary_key=True)
    parent_id = Column(Integer, ForeignKey('tag.id'), nullable=False)
    child_id = Column(Integer, ForeignKey('tag.id'), nullable=False)
    parent = relationship(Tag, foreign_keys=[parent_id],
                          backref=backref('hierarchyparent', cascade="all, delete-orphan"))
    child = relationship(Tag, foreign_keys=[child_id],
                         backref=backref('hierarchychild', cascade="all, delete-orphan"))


class RelationType:
    Tag = 0
    Category = 1


class Relation(Base):
    __tablename__ = 'relation'

    id = Column(Integer, primary_key=True)
    tag_id = Column(Integer, ForeignKey('tag.id'), nullable=False)
    object_id = Column(Integer, ForeignKey('fobject.id'), nullable=False)
    type = Column(Integer, default=RelationType.Tag)
    tag = relationship(Tag, foreign_keys=[tag_id],
                       backref=backref('relationtag', cascade="all, delete-orphan"))
    object = relationship(fObject, foreign_keys=[object_id],
                          backref=backref('relationobject', cascade="all, delete-orphan"))


Engine = create_engine('sqlite:///' + settings.DBFile)
Log.msg(Log.cf(), "database Engine initialized")

Base.metadata.create_all(Engine, checkfirst=True)
Log.msg(Log.cf(), "database Base Metadata initialized")
