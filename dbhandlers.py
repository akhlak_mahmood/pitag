import random

import database
from utils import *
import settings

def handle_addfile(cmd):
    # add a single file to database
    fname = parseCommand(cmd, 1).args[1]

    if fname.startswith(settings.ShortcutStackChar):
        fpath = idxStackFile(fname)
        fname = os.path.basename(fpath)
    else:
        fname = idxWorkFile(fname)
        fpath = os.path.join(settings.WorkDir, fname)

    if not os.path.isfile(fpath):
        database.deleteFile(fpath)
        database.commit()
        uprint(fpath, "updated in database")
    else:
        database.addFile(fpath)
        database.commit()
        uprint(fpath, "added")


def handle_tagfile(cmd):
    # add tags to a single file
    cmd = parseCommand(cmd, 2)
    fname = cmd.args[1]
    tags = cmd.args[2]

    if fname.startswith(settings.ShortcutStackChar):
        fpath = idxStackFile(fname)
        fname = os.path.basename(fpath)
    else:
        fname = idxWorkFile(fname)
        fpath = os.path.join(settings.WorkDir, fname)

    if os.path.isfile(fpath):
        fp = database.addFile(fpath)

        for t in tags.split():
            tp = database.addTag(t)
            database.addRelation(fp, tp)
            uprint("{} <- {}".format(fname, t))

        database.commit()
    else:
        error(cf(), "not a file {}", fpath)


def handle_copytags(cmd):
    cmd = parseCommand(cmd, 2)
    src = cmd.args[1]
    tar = cmd.args[2]

    if src.startswith(settings.ShortcutStackChar):
        srcpath = idxStackFile(src)
        src = os.path.basename(srcpath)
    else:
        src = idxWorkFile(src)
        srcpath = os.path.join(settings.WorkDir, src)

    if tar.startswith(settings.ShortcutStackChar):
        tarpath = idxStackFile(tar)
        tar = os.path.basename(tarpath)
    else:
        tar = idxWorkFile(tar)
        tarpath = os.path.join(settings.WorkDir, tar)

    if os.path.isfile(srcpath) and os.path.isfile(tarpath):
        tags = database.copytags(srcpath, tarpath)
        database.commit()
        for tag in tags:
            uprint("{} <- {}".format(tar, tag.name))
    else:
        error(cf(), "not valid files {} or {}", srcpath, tarpath)


def handle_tagall(cmd):
    # add tags to all files in the work dir
    tags = parseCommand(cmd, 1).args[1]
    for f in settings.WorkFiles:
        f = os.path.join(settings.WorkDir, f)
        fo = database.addFile(f)
        for t in tags.split():
            to = database.addTag(t)
            database.addRelation(fo, to)
    uprint("{} tags -> {} files".format(len(tags.split()), len(settings.WorkFiles)))
    database.commit()


def handle_tagallstack(cmd):
    # add tags to all files in the work dir
    tags = parseCommand(cmd, 1).args[1]
    for f in settings.UserStack:
        fo = database.addFile(f)
        for t in tags.split():
            to = database.addTag(t)
            database.addRelation(fo, to)
    uprint("{} tags -> {} files".format(len(tags.split()), len(settings.UserStack)))
    database.commit()


def handle_filetags(cmd):
    # print the tags of a file
    fname = parseCommand(cmd, 1).args[1]

    if fname.startswith(settings.ShortcutStackChar):
        fpath = idxStackFile(fname)
        fname = os.path.basename(fpath)
    else:
        fname = idxWorkFile(fname)
        fpath = os.path.join(settings.WorkDir, fname)

    if os.path.isfile(fpath):
        fileO = database.addFile(fpath)
        database.commit()

        tags = database.findTagsOfFile(fileO)
        for t in tags:
            print(t.name)
        say(cf(), "Total tags {}", len(tags))
    else:
        error(cf(), "not a file {}", fpath)


def handle_tagfiles(cmd):
    # print the files with tags
    tags = parseCommand(cmd, 1).args[1]

    for t in tags.split():
        for f in database.findFilesWithTag(t):
            fname = os.path.basename(f.path)
            addToStack(f.path)
    printStackFiles()


def handle_alltagsfiles(cmd):
    # print the files with all the tags
    tags = parseCommand(cmd, 1).args[1]
    found = database.findFilesWithAllTags(tags.split())
    for f in found:
        addToStack(f.path)
    printStackFiles()
    say(cf(), "FOUND {} files -> AND[{}]", len(found), tags)


def handle_anytagfiles(cmd):
    tags = parseCommand(cmd, 1).args[1]
    found = database.findFilesWithAnyTag(tags.split())
    for f in found:
        addToStack(f.path)
    printStackFiles()
    say(cf(), "FOUND {} files -> OR[{}]", len(found), tags)


def handle_showinfo():
    # print files and tags in database
    fc = database.allFiles()
    tc = database.allTags()

    if len(fc) > 20:
        print("\n20 RANDOM FILES:")
        sample = random.sample(fc, 20)
    else:
        sample = fc
        print("\nFILES:")

    for f in sample:
        fname = os.path.basename(f.path)
        uprint(fname)
        msg(cf(), f.hash)

    if len(tc) > 20:
        print("\n20 RANDOM TAGS:")
        sample = random.sample(tc, 20)
    else:
        sample = tc
        print("\nTAGS:")
    for t in sample:
        print(t.name)

    print("\nFiles: {} Tags: {}".format(len(fc), len(tc)))


def handle_showfileinfo(cmd):
    # print detailed info of a single file
    fname = parseCommand(cmd, 1).args[1]

    if fname.startswith(settings.ShortcutStackChar):
        fpath = idxStackFile(fname)
        fname = os.path.basename(fpath)
    else:
        fname = idxWorkFile(fname)
        fpath = os.path.join(settings.WorkDir, fname)

    say(cf(), "File Name: {}", fname)
    say(cf(), "File Path: {}", fpath)
    say(cf(), "File Hash: {}", quickHash(fpath))


    fileO = database.findFileWithPath(fpath)
    if not fileO:
        say(cf(), "File not in database.")
        return

    say(cf(),
        "\nIn database"
        "\nFile Id: {}"
        "\nFile Path: {}"
        "\nFile Hash: {}"
        "\nFile Added on: {}", fileO.id, fileO.path, fileO.hash, fileO.added)

    tagOs = database.findTagsOfPath(fpath)
    for tagO in tagOs:
        say(cf(), "Tag: {}", tagO.name)


def handle_deletetag(cmd):
    # delete tags of a single file
    cmd = parseCommand(cmd, 2)
    fname = cmd.args[1]
    tags = cmd.args[2]

    if fname.startswith(settings.ShortcutStackChar):
        fpath = idxStackFile(fname)
        fname = os.path.basename(fpath)
    else:
        fname = idxWorkFile(fname)
        fpath = os.path.join(settings.WorkDir, fname)

    if os.path.isfile(fpath):
        fileO = database.findFileWithPath(fpath)

        for t in tags.split():
            tagO = database.findTagWithName(t)
            database.deleteRelation(fileO, tagO)
            uprint("{} <-X- {}".format(fname, t))
        database.commit()
    else:
        error(cf(), "not a file {}", fpath)


def handle_refreshdb():
    files = database.allFiles()

    for fobj in files:
        if not os.path.isfile(fobj.path):
            database.deleteFile(fobj)
            uprint("Removed:", fobj.path)
        else:
            msg(cf(), "OK: {}", fobj.path)

    database.commit()
    print("Total files refreshed:", len(files))
